NAME:= usbhub-scan
INITRAMFS_DIR:= /etc/initramfs-tools
BIN_INSTALL_PATH:= /sbin/$(NAME)
HOOK_INSTALL_PATH:= $(INITRAMFS_DIR)/hooks/$(NAME)
SCRIPT_INSTALL_PATH:= $(INITRAMFS_DIR)/scripts/local-top/$(NAME)

.PHONY: install

$(NAME): main.c; $(CC) -o $@ $^ -ludev

install: $(BIN_INSTALL_PATH) $(HOOK_INSTALL_PATH) $(SCRIPT_INSTALL_PATH); update-initramfs -u
$(BIN_INSTALL_PATH): $(NAME); install -m 755 $< $@
$(HOOK_INSTALL_PATH): hook; install -m 755 $< $@
$(SCRIPT_INSTALL_PATH): script; install -m 755 $< $@
