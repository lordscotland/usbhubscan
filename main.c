#include <fcntl.h>
#include <libudev.h>
#include <stdio.h>
#include <unistd.h>

int main() {
  struct udev* udev=udev_new();
  if(!udev){return 1;}
  struct udev_enumerate* enumdev=udev_enumerate_new(udev);
  udev_enumerate_add_match_subsystem(enumdev,"usb");
  udev_enumerate_add_match_sysattr(enumdev,"bDeviceClass","09");
  udev_enumerate_add_nomatch_sysattr(enumdev,"bMaxPower","0mA");
  udev_enumerate_scan_devices(enumdev);
  struct udev_list_entry* entry0=udev_enumerate_get_list_entry(enumdev);
  struct udev_list_entry* entry;
  udev_list_entry_foreach(entry,entry0) {
    const char* syspath=udev_list_entry_get_name(entry);
    struct udev_device* device=udev_device_new_from_syspath(udev,syspath);
    const char* devnode=udev_device_get_devnode(device);
    if(devnode){
      int fd=open(devnode,O_WRONLY);
      printf("[%c] %s (%s:%s) %s\n",(fd!=-1)?'+':'-',devnode,
       udev_device_get_sysattr_value(device,"idVendor"),
       udev_device_get_sysattr_value(device,"idProduct"),
       udev_device_get_sysattr_value(device,"product"));
      if(fd!=-1){close(fd);}
    }
    udev_device_unref(device);
  }
  udev_enumerate_unref(enumdev);
  udev_unref(udev);
  return 0;       
}
